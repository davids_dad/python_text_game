#!/usr/bin/python3

def report_number_of_cards(packs):
  print(f"If I have {packs} packs of cards,")
  print(f"then I have {packs*52} cards.")
  print()


report_number_of_cards(packs=1)
report_number_of_cards(packs=3)
report_number_of_cards(packs=7)
