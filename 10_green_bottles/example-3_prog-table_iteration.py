#!/usr/bin/python3

for n in [2,3,5,7,11]:
  print (f"{n}*2 is {n*2}")

#Why is the first *2 printing *2, but the 2nd is doing a ×2?  
