#!/usr/bin/python3
#The line above tells a Unix machine (E.g. Gnu/Linux), what language this is writen in.

def print_green_bottles(number_of_bottles):
  print(f"{10} green bottles sitting on the wall")
  print(f"{10} green bottles sitting on the wall")
  print("and if one green bottle should accidently fall,")
  print(f"there'd be {9} green bottle sitting on the wall")
  print()

print_green_bottles(number_of_bottles=10)
print_green_bottles(number_of_bottles=9)

# Lines starting with a # are ignored by python, they are for you to read.

# Read procedure_example, run it, investigate it, and modify it. Until you grok how it works.
#Then modify this program, so that it does the first 2 verses. Only modify the bits in quotes (green, if using idle).

#Before adding the other verses. Read, run, investigate, then modify, the iteration examples. Use what you learn to do the other verses in one extra (and one less) lines.
