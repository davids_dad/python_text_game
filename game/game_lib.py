from game_data import *

##################################################################
# Code

current_room=garden
items_carried=[]
is_markdown_enabled=False

###############
#display

from IPython.display import display as disp, Markdown, Latex, HTML, clear_output
def display(md=""):
   disp(Markdown(md))

def clear():
  display()
  display("#"*40)
  clear_output()

def debug(msg=""):
  print(msg)

if not is_markdown_enabled:
  def Markdown(md):
    return md

  def clear():
     pass

################
#item

def _item_move(item, away_from, to):
  to.append(item)
  away_from.remove(item)

def item_move(item, away_from, to):
  if item_becomes_another_item_when_taken(item):
    new_item= item_becomes(item)
    old_item= item_requires(item)
    to.append(new_item)
    to.remove(old_item)
    away_from.remove(item)
  else:
    _item_move(item, away_from, to)

def item_requires_another_item(item):
  return "requires" in item

def item_becomes_another_item_when_taken(item):
  return "becomes" in item

def item_has_required(item):
  required_item_missing= item_requires_another_item(item) and not item_exists(items_carried, item["requires"])
  return not required_item_missing

def item_becomes(item):
  if item_becomes_another_item_when_taken(item):
    result= item["becomes"]
  else:
    result= None
  return result

def item_requires(item):
  if item_requires_another_item(item):
    result= item["requires"]
  else:
    result= None
  return result

#items

def item_exists(items, item):
  return item in items

def item_names(items):
  if len(items)>0:
    result= [item['name'] for item in items]
  else:
    result= "nothing"
  return result


def item_with_name(items, item_name):
  # this assumes that we have zero or one item with a name.
  # it will break if this assumption is not true.
  items= [item for item in items if item['name'] == item_name]
  if len(items)>0:
    result=items[0]
  else:
    result=None
  return result

def room_item_with_name(item_name):
   return item_with_name(items=current_room['items'], item_name=item_name)

def room_exit_with_name(item_name):
   #exits are a bit like items: they can exist, they have requirements
   return (
      item_with_name(items=current_room['exits'], item_name=item_name)
      if 'exits' in current_room else
      None
      )

################################################
# room

def show_room_describe(room):
  display(f"You are in {room['name']}.")
  display(f"You look around and see {room['description']}.")
  if "items" in room:
    display(f"These items are here: {item_names(room['items'])}.")
  if "exits" in room:
    display(f"Other places that you may be able to go: {item_names(room['exits'])}.")

##################################################
# commands

def show_inventary(_=None):
  display(f"You are carrying {item_names(items_carried)}.")

def look(_=None):
  show_room_describe(current_room)
  show_inventary()

def examine(item_name):
  item=item_with_name(items_carried, item_name)
  if item == None:
    display(f"You are not carrying a {item_name}.")
  else:
    display(f"You examine the {item_name}.")
    if "description" in item:
      display(f"{item['description']}")
    else:
      display(f"It is just a {item_name}.")

def take(item_name):
  item_to_take= room_item_with_name(item_name)
  if item_to_take == None:
    display(f"I see no {item_name} here.")
  else:
    if item_has_required(item=item_to_take):
      item_move(item=item_to_take, away_from=current_room['items'], to=items_carried)
      display(f"{item_to_take['name']} taken.")
      show_inventary()
    else:
      display(f"You can't pick up the {item_name} yet.")

def move(direction):
  global current_room
  new_room=room_exit_with_name(direction)
  if new_room == None:
    display(f"I see no {direction} here.")
  else:
    if item_has_required(item=new_room):
      current_room=new_room
      look()
    else:
      display(f"You can't enter the {direction} yet.")

def show_help(command=""):
  if command=="":
    command_list=list(commands.keys())
    display(f"Available commands: {command_list}")
    display(f"Type `help «command»` to get more help on a command")
  elif command in commands:
    if "help" in commands[command]:
      display(commands[command]["help"])
    else:
      display("Sorry no help for that command")

is_playing=True
def quit_game(_):
  global is_playing
  is_playing=False

commands={
  "look":     { "command": look,       "help": "`look` — used to see where you are"},
  "examine":  { "command": examine,    "help": "`examine «object»` — used to examine an object"},
  "take":     { "command": take,       "help": "`take «object»` — used to take an object"},
  "quit":     { "command": quit_game},
  "enter":    { "command": move,       "help": "`move «direction»` — used to move to a new place"},
  "help":     { "command": show_help,  "help": "`help` is helpful"},
  "inventary":{ "command": show_inventary, "help": "tells you what you are carrying"}, 
}

def play():
  look()
  while is_playing:
    display()
    display("Type `help` for help.")
    display("What do you want to do?")
    in_text=input()
    words=in_text.lower().split()
    if len(words) > 0:
      command_word=words[0]
      if command_word in commands:
        command=commands[command_word]["command"]
        arg=" ".join(words[1:])
        clear()
        command(arg)
      else:
        display(f"I don't know how to {command_word}")
