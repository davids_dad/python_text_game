# Objects

spoon= {"name":"spoon"}

key_1={
   "name":"house key",
   "description": "the key that you found under a pile of dirt"
}

dirt={
   "name": "dirt",
   "description":"a pile of dirt",
   "requires": spoon,
   "becomes": key_1,
}

bucket= {
  "name":"pink bucket",
  "description": "An old bucket. You could carry things in it."
}

bucket_of_water={
  "name": "bucket of water",
  "description": "This is the bucket that you filled with water. The water is wet, and good to swim in, but you would not fit in this bucket."
}

water={
  "name":"water",
  "requires": bucket,
  "becomes": bucket_of_water,
  "description":"some cool wet water, you could take some if you had something to carry it.", 
}

bread={
   "name":"bread",
}

fish_in_a_bucket={
   "name": "fish",
   "description": "the fish that you found. It is swimming around in the pink bucket",
}

fish={
   "name": "fish",
   "description": "this fish looks like it wants to get out of here",
   "requires": bucket_of_water,
   "becomes": fish_in_a_bucket,
}

stick={
    "name": "stick",
    "description": "the stick that has not yet found a use in the game",
}

key_2={
    "name": "key",
    "description":"the key that you felt with your toes, in the dark cave",
}

# Rooms

cave={
    "name": "the cave",
    "description": "it is dark, you see nothing",
    "items":[
        key_2
    ],
    "exits":[]
}   

kitchen={
   "name": "the kitchen",
   "description": "this is where the food is prepared",
   "items": [
      fish, bread,
   ],
   "exits":[

   ]
}

guest_room={
   "name": "guest room",
   "description": "this is where guests are entertained. The door closes behind you, it is locked",
   "items":[

   ],
   "exits":[

   ]
}

entrance_hall={
   "name": "house",
   "description": "the entrance hall to a big house",
   "requires": key_1,
   "exits": [
      kitchen, guest_room
   ]
}

garden={
   "name": "the garden",
   "description": "a green plesent place",
   "items": [
      bucket, water, spoon, dirt,
   ],
   "exits": [
      entrance_hall
   ]
}

guest_room["exits"].append(garden)
kitchen["exits"].append(entrance_hall)
cave["exits"].append(garden)
