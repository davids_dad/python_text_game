#!/usr/bin/python3

hero="Alice"
sister="Bob"
villain="Charles"
country="Denmark"
town="Eversley"
town2="Farnham"
friend="snowman"

print(
  f"""
    <h2>Part 1</h2>

    In a faraway land called {country}.
    There was a magical and mysterious castle in the town of {town}.
    It was said that behind the castle walls was a magical princess called {hero}.
    {hero} tried to hide her powers from the people of {town}, but there was curious prince called Prince {villain} who found out about these powers and forced {hero} to run away.

    <h2>Part2</h2>

    {hero} ran way, and imposed an endless winter on {town}.
    Her sister {sister} set off on a dangerous adventure to help {hero} return to {town}.

    <h2>Part 3</h2>

    {sister} found her way to {town2} where her sister had used her magical powers to build a palace of ice.
    Along the way, {sister} had to overcome many challenges and met a new {friend} friend.

    <h2>Part 4</h2>

    {sister} and {hero} work together to put the nasty {villain} in jail.
    {hero} and {sister} returned to {town} to save {country} from an eternal winter.
  """
)

#Note we can not get the school computers to do markdown to html: No control over what we install, not Unix, etc.
