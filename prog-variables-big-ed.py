#!/usr/bin/python3

from math import pi

def ask_name():
  global name
  name=input("What is your name?")
  print(f"Hello {name}")

def ask_music():
  music_preference = input(f"Hi {name}, it is Ed here. What type of music do you like?")
  print(f"I also like to listen to ....")

def ask_number():
  best_number=input(f"{name}, what is your favourite number?")
  print(f"... is a good number. My favourite is 2π, it is approximately {2*pi}")

def ask_age():
  print(f"What is your age?")

ask_name()
#ask_music()
#ask_number()
#ask_age()
